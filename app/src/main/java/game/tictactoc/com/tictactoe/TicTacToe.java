package game.tictactoc.com.tictactoe;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by rohitsoni on 04/09/17.
 */

public class TicTacToe extends AppCompatActivity implements View.OnClickListener {

    private int mCounter;
    private int mPosition;
    private Button mUser11, mUser12, mUser13;
    private Button mUser21, mUser22, mUser23;
    private Button mUser31, mUser32, mUser33;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tictactoe);

        mUser11 = (Button) findViewById(R.id.user_option_button_1_1);
        mUser11.setOnClickListener(this);
        mUser12 = (Button) findViewById(R.id.user_option_button_1_2);
        mUser12.setOnClickListener(this);
        mUser13 = (Button) findViewById(R.id.user_option_button_1_3);
        mUser13.setOnClickListener(this);
        mUser21 = (Button) findViewById(R.id.user_option_button_2_1);
        mUser21.setOnClickListener(this);
        mUser22 = (Button) findViewById(R.id.user_option_button_2_2);
        mUser22.setOnClickListener(this);
        mUser23 = (Button) findViewById(R.id.user_option_button_2_3);
        mUser23.setOnClickListener(this);
        mUser31 = (Button) findViewById(R.id.user_option_button_3_1);
        mUser31.setOnClickListener(this);
        mUser32 = (Button) findViewById(R.id.user_option_button_3_2);
        mUser32.setOnClickListener(this);
        mUser33 = (Button) findViewById(R.id.user_option_button_3_3);
        mUser33.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.user_option_button_1_1:
                setXAndO(mUser11);
                mPosition = 1;
                break;
            case R.id.user_option_button_1_2:
                setXAndO(mUser12);
                mPosition = 2;
                break;
            case R.id.user_option_button_1_3:
                setXAndO(mUser13);
                mPosition = 3;
                break;
            case R.id.user_option_button_2_1:
                setXAndO(mUser21);
                mPosition = 4;
                break;
            case R.id.user_option_button_2_2:
                setXAndO(mUser22);
                mPosition = 5;
                break;
            case R.id.user_option_button_2_3:
                setXAndO(mUser23);
                mPosition = 6;
                break;
            case R.id.user_option_button_3_1:
                setXAndO(mUser31);
                mPosition = 7;
                break;
            case R.id.user_option_button_3_2:
                setXAndO(mUser32);
                mPosition = 8;
                break;
            case R.id.user_option_button_3_3:
                setXAndO(mUser33);
                mPosition = 9;
                break;
        }

        if (mCounter >= 5 && isGameOver(mPosition)) {
            if (mCounter % 2 == 0) {
                showToast("Player2 won the game");
            } else {
                showToast("Player1 won the game");
            }
            disableTouch();
        } else if (mCounter == 9) {
            showToast("Its Draw");
            disableTouch();
        }
    }

    private void setXAndO(Button b) {
        if (b.getText().toString().length() == 0) {
            if (mCounter % 2 == 0) {
                b.setText("X");
            } else {
                b.setText("0");
            }
            mCounter++;
        }
    }

    private boolean isGameOver(int position) {

        String data11 = mUser11.getText().toString();
        String data12 = mUser12.getText().toString();
        String data13 = mUser13.getText().toString();

        String data21 = mUser21.getText().toString();
        String data22 = mUser22.getText().toString();
        String data23 = mUser23.getText().toString();

        String data31 = mUser31.getText().toString();
        String data32 = mUser32.getText().toString();
        String data33 = mUser33.getText().toString();

        switch (position) {

            case 1:
                if (data11.equals(data12) && data11.equals(data13)) {
                    return true;
                }
                if (data11.equals(data21) && data11.equals(data31)) {
                    return true;
                }
                if (data11.equals(data22) && data11.equals(data33)) {
                    return true;
                }
                break;
            case 2:
                if (data12.equals(data11) && data12.equals(data13)) {
                    return true;
                }
                if (data12.equals(data22) && data12.equals(data32)) {
                    return true;
                }
                break;
            case 3:
                if (data13.equals(data11) && data13.equals(data12)) {
                    return true;
                }
                if (data13.equals(data22) && data13.equals(data31)) {
                    return true;
                }
                if (data13.equals(data23) && data13.equals(data33)) {
                    return true;
                }
                break;
            case 4:
                if (data21.equals(data11) && data21.equals(data31)) {
                    return true;
                }
                if (data21.equals(data22) && data21.equals(data23)) {
                    return true;
                }
                break;
            case 5:
                if (data22.equals(data12) && data22.equals(data32)) {
                    return true;
                }
                if (data22.equals(data21) && data22.equals(data23)) {
                    return true;
                }
                if (data22.equals(data11) && data22.equals(data33)) {
                    return true;
                }
                if (data22.equals(data13) && data22.equals(data31)) {
                    return true;
                }
                break;
            case 6:
                if (data23.equals(data13) && data23.equals(data33)) {
                    return true;
                }
                if (data23.equals(data21) && data23.equals(data22)) {
                    return true;
                }
                break;
            case 7:
                if (data31.equals(data11) && data31.equals(data21)) {
                    return true;
                }
                if (data31.equals(data32) && data31.equals(data33)) {
                    return true;
                }
                if (data31.equals(data22) && data31.equals(data13)) {
                    return true;
                }
                break;
            case 8:
                if (data32.equals(data12) && data32.equals(data22)) {
                    return true;
                }
                if (data32.equals(data31) && data32.equals(data33)) {
                    return true;
                }
                break;
            case 9:
                if (data33.equals(data11) && data33.equals(data22)) {
                    return true;
                }
                if (data33.equals(data13) && data33.equals(data23)) {
                    return true;
                }
                if (data33.equals(data31) && data33.equals(data32)) {
                    return true;
                }
                break;

        }

        return false;
    }

    private void showToast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    private void disableTouch() {
        mUser11.setOnClickListener(null);
        mUser12.setOnClickListener(null);
        mUser13.setOnClickListener(null);

        mUser21.setOnClickListener(null);
        mUser22.setOnClickListener(null);
        mUser23.setOnClickListener(null);

        mUser31.setOnClickListener(null);
        mUser32.setOnClickListener(null);
        mUser33.setOnClickListener(null);
    }
}
